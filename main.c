#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <termbox.h>

int main(){

    tb_init();	//INITWIN
    
    struct tb_event input;	//INPUT ABFRAGE
    
    struct tb_cell c;		//PLAYER
    c.ch = 'O';				//--PLAYER.ZEICHEN
    c.fg = TB_WHITE;		//--PLAYER.ZEICHENFARBE
    c.bg = TB_RED;			//--PLAYER.HINTERGRUNDFARBE
    
    int x = tb_width()/2;	//WEITE MEINES FENSTERS HORIZONTAL / 2 IST x POSITION VON PLAYER c
    int y = tb_height()-3;	//GRÖßE MEINES FENSTERS VERTIKAL / 2 IST y POSITION VON PLAYER c
    
    
    tb_put_cell(x,y,&c);	//VORBEREITUNG FÜR ANZEIGE VON PLAYER (ANZEIGE DURCH PRESENT)
    tb_present();			//JETZIGEN ZUSTAND ANZEIGEN
    
    struct tb_cell p;
    c.ch = '|';
    c.fg = TB_WHITE;
    c.bg = TB_RED;
        
        
    do
    {
        tb_poll_event(&input);	//HOLE INPUT
        
        switch(input.key){
        	case TB_KEY_ARROW_LEFT:
        		if(x!=3){
	        		x--;
        		}				//X POSITION UM 1 nach LINKS VERSCHIEBEN
        		tb_clear();				//CLEAR
        		tb_put_cell(x,y,&c);	//SETZE PLAYER c AUF NEUE POSITION
				tb_present();			//REFRESH
        		continue;
        	case TB_KEY_ARROW_RIGHT:
        		if(x!=tb_width()-3){
        			x++;		//X POSITION UM 1 nach RECHTS VERSCHIEBEN
        		}
        		tb_clear();				//CLEAR
				tb_put_cell(x,y,&c);	//SETZE PLAYER c AUF NEUE POSITION
				tb_present();			//REFRESH
        		continue;
        	case TB_KEY_SPACE:
        		shoot(x,y);
        }
    } while(input.key != TB_KEY_ENTER);
    
    tb_shutdown();	//ENDWIN
    return 0;
}

void shoot(int x, int y){

		

}